import axios from 'axios';

const apiURL = process.env.VUE_APP_EXCHANGE_API_URL;

export default {
  async loadCurrencyList(ctx) {
    const data = await ctx.dispatch('loadLatestCurrExchangeFromAPI');
    const currencyList = Object.keys(data.rates);
    currencyList.push(data.base);
    ctx.commit('setCurrencyList', currencyList.sort());
  },

  async loadLatestCurrExchangeFromAPI(ctx, payload) {
    const resp = await axios.get(`${apiURL}/latest`, {
      params: payload,
    });
    return resp.data;
  },

  async loadHistoryCurrExchangeAPI(ctx, payload) {
    const resp = await axios.get(`${apiURL}/history`, {
      params: payload,
    });
    return resp.data;
  },
};
