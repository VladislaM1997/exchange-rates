import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/pages/Home';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/404',
    name: 'Page404',
    component: () => import('../views/pages/Page404'),
  },
  {
    path: '**',
    redirect: '/404',
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: 'nav-link__exact-active',
  routes,
});

export default router;
